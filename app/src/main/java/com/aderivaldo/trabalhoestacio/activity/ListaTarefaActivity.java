package com.aderivaldo.trabalhoestacio.activity;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ListView;


import com.aderivaldo.trabalhoestacio.R;
import com.aderivaldo.trabalhoestacio.model.Task;
import com.aderivaldo.trabalhoestacio.util.DbOperation;

import java.util.List;


public class ListaTarefaActivity extends AppCompatActivity {

    ListView tarefasListView;
    TarefasAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_tarefa);
        tarefasListView = (ListView) findViewById(R.id.lista_tarefas);

        DbOperation op = new DbOperation(this.getApplicationContext());
        op.getAllTasks(new DbOperation.DBOperationsTaskCallBack() {
            @Override
            public void taskSaved(Boolean success) {}

            @Override
            public void getAllTasks(final List<Task> listaTarefas) {
                ListaTarefaActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter = new TarefasAdapter(ListaTarefaActivity.this, 1, listaTarefas);
                        tarefasListView.setAdapter(adapter);
                    }
                });
            }
        });




    }
    @Override
    public boolean onCreateOptionsMenu (Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_tarefas, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        if(item.getItemId() == R.id.adicionar_tarefa){
            criarDialogoTarefa();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void criarDialogoTarefa(){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setMessage("Coloque a descrição da tarefa");
        alert.setTitle("Criar tarefa");
        alert.setCancelable(true);

        final EditText editText = new EditText(getApplicationContext());
        alert.setView(editText);

        alert.setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int whichButton) {
                String taskTitle = editText.getText().toString();
               final Task novaTarefa = new Task(taskTitle);
                DbOperation db = new DbOperation(getApplicationContext());
                db.saveTask(new Task(taskTitle), new DbOperation.DBOperationsTaskCallBack() {
                    @Override
                    public void taskSaved(Boolean success) {
                        ListaTarefaActivity.this.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.add(novaTarefa);
                            }
                        });
                    }

                    @Override
                    public void getAllTasks(List<Task> listaTarefas) {

                    }
                });
            }
        });

        alert.setNegativeButton("Cancelar", null);
        alert.show();
    }

}


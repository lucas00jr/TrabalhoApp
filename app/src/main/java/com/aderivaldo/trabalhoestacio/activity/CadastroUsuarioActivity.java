package com.aderivaldo.trabalhoestacio.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.aderivaldo.trabalhoestacio.R;
import com.aderivaldo.trabalhoestacio.model.User;
import com.aderivaldo.trabalhoestacio.util.DbOperation;

public class CadastroUsuarioActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastro_usuario);
    }

    public void cadastrar(View view) {
        EditText newUsername = (EditText) findViewById(R.id.new_username);
        String username = newUsername.getText().toString();

        EditText newPassword = (EditText) findViewById(R.id.new_password);
        String password = newPassword.getText().toString();

        DbOperation db = new DbOperation(this);
        db.saveUser(new User(username, password), new DbOperation.DBOperationsCallBack() {
            @Override
            public void userSaverd(Boolean success) {
                CadastroUsuarioActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(CadastroUsuarioActivity.this, "Usuario salvo!", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            @Override
            public void userExists(Boolean exists) {

            }

        });
    }
}

package com.aderivaldo.trabalhoestacio.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.aderivaldo.trabalhoestacio.R;
import com.aderivaldo.trabalhoestacio.util.DbOperation;



public class MainActivity extends AppCompatActivity {
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    private void startProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Carregando");
        progressDialog.show();

    }

    private void stopProgressDialog() {
        progressDialog.dismiss();
    }

    public void login(View view) {
        Intent intent = new Intent(this, ListaTarefaActivity.class);
        startActivity(intent);
    }
    /*
        EditText username = (EditText) findViewById(R.id.username);
        EditText password = (EditText) findViewById(R.id.password);

        String user =  username.getText().toString();
        String pass =  password.getText().toString();

        DbOperation dbOperations = new DbOperation(this);
        dbOperations.queryAuthenticatedUser(user, pass, new DbOperation.DBOperationsCallBack() {
            @Override
            public void userSaverd(Boolean success) {}

            @Override
            public void userExists(final Boolean exists) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(exists) {
                            Toast.makeText(MainActivity.this, "Usuario logado!!!", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(MainActivity.this, "Usuario não logado!!!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }

            @Override
            public void userUpdated(Boolean success) {}

            @Override
            public void userDeleted(Boolean success) {}
        });
    }
*/
    public void cadastrar(View view) {
        Intent intent1 = new Intent(this, CadastroUsuarioActivity.class);
        startActivity(intent1);
    }
}

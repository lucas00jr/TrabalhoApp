package com.aderivaldo.trabalhoestacio.util;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.aderivaldo.trabalhoestacio.dao.TaskDao;
import com.aderivaldo.trabalhoestacio.dao.UserDao;
import com.aderivaldo.trabalhoestacio.model.Task;
import com.aderivaldo.trabalhoestacio.model.User;


@Database(entities = {User.class, Task.class}, version = 2)
public abstract class DataBase extends RoomDatabase {
    private static final String databaseName = "estacio_app";

    private static DataBase instance;
    public abstract UserDao userDao();
    public abstract TaskDao taskDao();

    public static DataBase getInstance(Context context) {
        if (instance == null || !instance.isOpen()) {
            instance = Room.databaseBuilder(context, DataBase.class, databaseName).build();
        }
        return instance;
    }
}
